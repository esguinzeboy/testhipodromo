# Test Hipódromo
## _Test desarrollador .net_

Realización de Test para desarrollador .net hipdoromo Palermo.

## Consideraciones Generales

- Utilice la arquitectura propuesta por Microsoft la cual existe una capa core, que todos la conocen, pero ella no conoce a ninguna.
- Como el ejercicio no solicita persistir datos. La capa de infrastrucutra propuesta por Microsoft no se realiza, pero tranquilamente se podría agregar.
- El ejercicio fue realizado en .Net Core 5
- Se utilizo una liberia externa llamada Jace. para el calculo de las formulas.
- El ejercicio no posee interfza solo es una aplicación de consola que carga los caballos y los muestra por pantalla.
- Se dejan algunas estructuras preparadas para darle mas complejidad a la consola, pero no se profundiza en ello.

## Breve Explicación del funcionamiento del programa.

- El programa al inciarse crea 5 caballos que corren una carrera. 
- Este Instancia una clase llamada RaceService.
- La misma posee un método que inicia una carrera.
- Los caballos son creado como Dtos que se tranfieren a la capa de servicio y esta los mapea a entidades. 
- En este punto, segun lo especificado en los enunciados del ejercicio, se setean los valores para los caballos.
- Se hacen los cálculos de cuanto tardarían en llegar a la meta.
- Por ultimo se muestran en pantalla.

> Note: Se crean algunas estructuras que no no son utilizdas en esta primera iteracion, pero se dejan para darle mayor complejidad en un futuro a la aplicacion, si es necesario.

## Tabla de Contenidos

| Versión |Descripción| Realizador | Fecha|
| ------ | ------ | ---|-----|
| 1.00 | [Versión Inicial | Ferreccio Javier Ezequiel| 20211203|

