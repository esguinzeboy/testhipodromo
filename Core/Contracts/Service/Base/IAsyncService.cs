﻿using Core.Entities.Contracts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Grip.Core.Contracts.Services
{
    public interface IAsyncService<TDto, TEntity, TIdentifier>
        where TDto : class
        where TEntity : Entity<TIdentifier>
    {
        Task<TDto> Get(TIdentifier id);
        Task<TDto> Get(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TDto>> GetAll(Expression<Func<TEntity, bool>> predicate = null);
        Task<IEnumerable<TProjected>> GetAll<TProjected>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TProjected>> projection);
        //Task<PagedDataDto<TDto>> GetPaged(int pageIndex, int pageSize, string sortExpression, string filterExpression);        
        Task<TDto> Add(TDto model);
        Task Update(TDto model);
        Task Delete(TIdentifier id);
    }
}
