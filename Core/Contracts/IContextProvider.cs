﻿namespace Grip.Core.Contracts
{
    public interface IContextProvider
    {
        string Username { get; }
        string Email { get; }
        string FullUsername { get; }
        int UserId { get; }
        int? TenantId { get; }
        int TerritoryId { get; }
    }
}
