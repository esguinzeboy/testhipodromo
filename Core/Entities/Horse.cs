﻿using Core.Entities.Contracts;
using Core.Enums;
using System.Drawing;

namespace Core.Entities
{
    public class Horse : Entity<int>
    {
        public int Number { get; set; }
        public bool IsMale { get; set; }
        public Color HairColor { get; set; } //podri usarse un string y grabar en hexa
        public double MaxSpeed { get; set; }
        public int Endurance { get; set; }
        public double Acceleration { get; set; }
        public double PureBlood { get; set; }
        public Gallop Gallop { get; set; }
        public RaceEnum Race { get; set; }
    }
}
