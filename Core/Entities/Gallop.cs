﻿using Core.Enums;

namespace Core.Entities.Contracts
{
    public class Gallop : Entity<int>
    {
        public string Description { get; set; }
        public string Formula { get; set; }
        public GallopEnum GallopEnum { get; set; }

    }
}
