﻿namespace Core.Entities.Contracts
{
    public class Entity<TIdentifier>
    {
        public TIdentifier Id { get; set; }
    }
}