﻿using Core.Entities;
using Core.Entities.Contracts;
using Core.Enums;
using System;
using System.Drawing;

namespace Core.Services
{
    public class HorseGeneratorService
    {
        public Horse CreateHorseAndAddBasicParameterToHorse(RaceEnum race)
        {
            var horse = new Horse();

            switch (race)
            {
                case RaceEnum.Creole:
                    horse.HairColor = Color.Brown;
                    horse.MaxSpeed = 10;
                    horse.Acceleration = 0.75;
                    horse.Gallop = GenerateGallop(GallopEnum.Furious);
                    break;
                case RaceEnum.Spc:
                    horse.HairColor = Color.Gold;
                    horse.MaxSpeed = 20;
                    horse.Acceleration = 3;
                    horse.Gallop = GenerateGallop(GallopEnum.Fast);
                    break;
                case RaceEnum.Arab:
                    horse.HairColor = Color.Black;
                    horse.MaxSpeed = 15;
                    horse.Acceleration = 1;
                    horse.Gallop = GenerateGallop(GallopEnum.Pretty);
                    break;
                default:
                    var rand = new Random();
                    horse.HairColor = Color.Gray;
                    horse.MaxSpeed = rand.Next(0, 20);
                    horse.Acceleration = rand.Next(50, 500) / 100;
                    horse.Gallop = GenerateGallop((GallopEnum)rand.Next(0, Enum.GetNames(typeof(GallopEnum)).Length));
                    break;
            }

            return horse;
        }


        public Horse CreateHorseAndAddBasicParameterToHorse(Color hairColor, double maxSpeed, double acceleration, GallopEnum gallopEnum)
        {
            var horse = new Horse();

            horse.HairColor = hairColor;
            horse.MaxSpeed = maxSpeed;
            horse.Acceleration = acceleration;
            horse.Gallop = GenerateGallop(gallopEnum);
            horse.Race = RaceEnum.Other;

            return horse;
        }




        private Gallop GenerateGallop(GallopEnum gallop)
        {
            var gallopToReturn = new Gallop();

            switch (gallop)
            {
                case GallopEnum.Pretty:
                    gallopToReturn.Description = "Pretty";
                    gallopToReturn.Formula = "X(t) = Xi + Vi * t + (a * t^2) / 2";
                    break;
                case GallopEnum.Furious:
                    gallopToReturn.Description = "Furious";
                    gallopToReturn.Formula = "X(t) = Xi + Vi * t + (a * t^2) / 4";
                    break;
                case GallopEnum.Fast:
                    gallopToReturn.Description = "Fast";
                    gallopToReturn.Formula = "X(t) = Xi + Vi * t * 2 + (a * t^2) / 2";
                    break;
                default:
                    //TODO Throw new bussines Exceptions
                    break;
            }

            gallopToReturn.GallopEnum = gallop;
            return gallopToReturn;


        }
    }
}
