﻿using Core.Dtos;
using Core.Entities;
using Core.Entities.Contracts;
using Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services
{
    public class RaceService
    {
        private readonly double _distance;
        public RaceService(double distance) 
        {
            _distance = distance;
        }
        public ResultOfRaceDto StartRace(List<HorseCreateDto> horsesDtos, int numberOfRace)
        {
            
            var listOfHorses = new List<Horse>();
            var hgs = new HorseGeneratorService();
            var number = 0;
            foreach (var horseDto in horsesDtos)
            {
                number++;
                var horseToAdd = hgs.CreateHorseAndAddBasicParameterToHorse(horseDto.Race);
                horseToAdd.Endurance = horseDto.Endurance;
                horseToAdd.IsMale = horseDto.IsMale;
                horseToAdd.PureBlood = horseDto.PureBlood;
                horseToAdd.Number = number;
                listOfHorses.Add(horseToAdd);
            }

            var listOfHorsesDto = new List<HorseDto>();
            foreach (var horse in listOfHorses)
            {

                listOfHorsesDto.Add(CalculateHorsePerformance(horse));

            }

            listOfHorsesDto.OrderBy(x => x.FinishTimeInSeconds);

            return new ResultOfRaceDto()
            {
                HorsePositions = listOfHorsesDto.OrderBy(x => x.FinishTimeInSeconds).ToList(),
                RaceNumber = numberOfRace
            };
        }

        private HorseDto CalculateHorsePerformance(Horse horse)
        {

            var xi = 0;
            var vi = 0;
            var a = horse.Acceleration;
            var maxSpeed = horse.MaxSpeed * horse.PureBlood;
            var timeToGetToMaxSpeed = GettimeToGetToMaxSpeed(a, maxSpeed, horse.Gallop.GallopEnum);
            var t = timeToGetToMaxSpeed;

            var distanceWhenAccelatret = CalculateFirstDistanceWhenAccelarte(xi, vi, t, a, horse.Gallop.Formula);
            var secondsFromTheRestOfDistance = CalculateSecondDitanceMaxSpeedAchieved(distanceWhenAccelatret, maxSpeed);

            return new HorseDto()
            {
                FinishTimeInSeconds = timeToGetToMaxSpeed + secondsFromTheRestOfDistance,
                Number= horse.Number,
                  
            };

            
        }

        private double CalculateSecondDitanceMaxSpeedAchieved(double distanceWhenAccelatret, double maxSpeed)
        {
            var distanceToUseInCalculate = _distance - distanceWhenAccelatret;
            return  distanceToUseInCalculate / maxSpeed;
        }

        private double CalculateFirstDistanceWhenAccelarte(int xi, int vi, double t, double a, string formula)
        {
            var calculator = new Jace.CalculationEngine();
            var formulaSplited = formula.Split("=");
            formulaSplited[1]=formulaSplited[1].Replace("Xi" , xi.ToString());
            formulaSplited[1]=formulaSplited[1].Replace("Vi", vi.ToString());
            formulaSplited[1]=formulaSplited[1].Replace("t" , t.ToString());
            formulaSplited[1]=formulaSplited[1].Replace("a" , a.ToString());

            var valueToReturn = calculator.Calculate(formulaSplited[1]);
            return valueToReturn;
            

        }

        private double GettimeToGetToMaxSpeed(double a, double maxSpeed, GallopEnum gallopEnum)
        {
            //TODO--> derivar formula de gallog
           
            switch (gallopEnum)
            {
                case GallopEnum.Pretty:
                    return maxSpeed / a;
                    break;
                case GallopEnum.Furious:
                    return (maxSpeed *2 / a);
                    break;
                case GallopEnum.Fast:
                    return (maxSpeed / a) ;
                    break;
                default:
                    return maxSpeed / a;
                    break;
            }
        }


    }
}
