﻿namespace Core.Dtos
{
    public class HorseDto
    {
        public int Number { get; set; }
        public int Position { get; set; }
        public double FinishTimeInSeconds { get; set; }
    }
}
