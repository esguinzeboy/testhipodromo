﻿using Core.Entities.Contracts;
using Core.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Dtos
{
    public class HorseCreateDto
    {

       public bool IsMale { get; set; }
       public int Endurance { get; set; }
       public double PureBlood { get; set; }
       public RaceEnum Race { get; set; }
    }
}
