﻿using System.Collections.Generic;

namespace Core.Dtos
{
    public class ResultOfRaceDto
    {
        public List<HorseDto> HorsePositions { get; set; }
        public int RaceNumber { get; set; }
    }
}
