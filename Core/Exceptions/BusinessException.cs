﻿namespace Grip.Core.Exceptions
{
    public class BusinessException : System.Exception
    {
        public BusinessException() {}

        public BusinessException(string businessRuleErrCode)
            : base(businessRuleErrCode)
        { }

        public BusinessException(string businessRuleErrCode, System.Exception innerException)
            : base(businessRuleErrCode, innerException)
        { }
    }
}
