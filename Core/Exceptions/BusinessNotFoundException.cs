﻿namespace Grip.Core.Exceptions
{

   public class BusinessNotFoundException : System.Exception 
   {
        public BusinessNotFoundException() { }

        public BusinessNotFoundException(string businessRuleErrCode)
            : base(businessRuleErrCode)
        { }

        public BusinessNotFoundException(string businessRuleErrCode, System.Exception innerException)
            : base(businessRuleErrCode, innerException)
        { }
    }
}