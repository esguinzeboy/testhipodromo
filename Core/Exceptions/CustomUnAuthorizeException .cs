﻿namespace Grip.Core.Exceptions
{
    public class CustomUnAuthorizeException : System.Exception
    {
        public bool IsManaged { get; set; }

        public CustomUnAuthorizeException() {}

        public CustomUnAuthorizeException(string businessRuleErrCode)
            : base(businessRuleErrCode)
        { }

        public CustomUnAuthorizeException(string businessRuleErrCode, bool isManaged)
            : base(businessRuleErrCode)
        {

            IsManaged = isManaged;
        }

        public CustomUnAuthorizeException(string businessRuleErrCode, System.Exception innerException)
            : base(businessRuleErrCode, innerException)
        { }
    }
}
