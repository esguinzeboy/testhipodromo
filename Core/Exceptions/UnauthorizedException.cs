﻿namespace Grip.Core.Exceptions
{
    public class UnauthorizedException : System.Exception
    {
        public bool IsManaged { get; set; }

        public UnauthorizedException() {}

        public UnauthorizedException(string businessRuleErrCode)
            : base(businessRuleErrCode)
        { }

        public UnauthorizedException(string businessRuleErrCode, bool isManaged)
            : base(businessRuleErrCode)
        {

            IsManaged = isManaged;
        }

        public UnauthorizedException(string businessRuleErrCode, System.Exception innerException)
            : base(businessRuleErrCode, innerException)
        { }
    }
}
