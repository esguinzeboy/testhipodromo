﻿namespace Grip.Core.Exceptions
{
    public class ForbiddenException : System.Exception
    {
        public bool IsManaged { get; set; }

        public ForbiddenException() {}

        public ForbiddenException(string businessRuleErrCode)
            : base(businessRuleErrCode)
        { }

        public ForbiddenException(string businessRuleErrCode, bool isManaged)
            : base(businessRuleErrCode)
        {

            IsManaged = isManaged;
        }

        public ForbiddenException(string businessRuleErrCode, System.Exception innerException)
            : base(businessRuleErrCode, innerException)
        { }
    }
}
