﻿using System;
using System.Text;

namespace Grip.Core.Exceptions
{
    public static class ExceptionExtensions
    {
        public static string GetDetailedMessage(this System.Exception ex, bool getCompleteInnerExceptionStack = true)
        {
            var _sbBuffExMessage = new StringBuilder();

            _sbBuffExMessage.AppendFormat("- Error: {0}", ex.Message);
            _sbBuffExMessage.AppendLine();
            _sbBuffExMessage.AppendFormat("- Stack: {0}", ex.StackTrace);

            var _innerException = ex.InnerException;

            while (_innerException != null)
            {
                _sbBuffExMessage.AppendLine();
                _sbBuffExMessage.AppendFormat("- Inner Exception: {0}", _innerException.Message);
                _sbBuffExMessage.AppendLine();
                _sbBuffExMessage.AppendFormat("- Stack: {0}", _innerException.StackTrace);
                _sbBuffExMessage.AppendLine();
                _sbBuffExMessage.AppendLine(new String("#".ToCharArray()[0], 100));

                if (!getCompleteInnerExceptionStack)
                    break;

                _innerException = _innerException.InnerException;
            }

            return _sbBuffExMessage.ToString();
        }
    }
}
