﻿using Core.Dtos;
using Core.Enums;
using Core.Services;
using System;
using System.Collections.Generic;

namespace TestHipodromo
{
    class Program
    {
        static void Main(string[] args)
        {
            var raceService = new RaceService(4000);
            var listHorseCreateDto = new List<HorseCreateDto>();

            listHorseCreateDto.Add(new HorseCreateDto() {Endurance = 02, IsMale = true, PureBlood = 0.763, Race = RaceEnum.Creole });
            listHorseCreateDto.Add(new HorseCreateDto() {Endurance = 05, IsMale = true, PureBlood = 0.373, Race = RaceEnum.Spc });
            listHorseCreateDto.Add(new HorseCreateDto() {Endurance = 01, IsMale = true, PureBlood = 0.513, Race = RaceEnum.Arab });
            listHorseCreateDto.Add(new HorseCreateDto() {Endurance = 10, IsMale = true, PureBlood = 0.3635, Race = RaceEnum.Spc });
            listHorseCreateDto.Add(new HorseCreateDto() {Endurance = 10, IsMale = true, PureBlood = 0.772, Race = RaceEnum.Creole });

            var resultOfRace = raceService.StartRace(listHorseCreateDto, 1);
            var position = 0;
            foreach (var horse in resultOfRace.HorsePositions)
            {
                position++;
                Console.WriteLine($"Posición: {position}: Caballo Nº{horse.Number} - tiempo {(int)horse.FinishTimeInSeconds} segundos.");
            }

        }
    }
}
